# this script downloads the default policies from upstream, adds IG BvC specific content and adds it to the policy set

import urllib.request
import json
import yaml
import logging, sys
import os.path

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

configfile = './policy-spec.yaml'
policy_dir = './policies/'
bundle_filename = './bundle.json'

with open(configfile, 'r') as config:
    mirror_config = yaml.safe_load(config)
    logging.debug(mirror_config)

# we set default settings for the policies
policy_disabled_default = mirror_config["defaults"]["policyDisabled"]

def fix_bsi_category(content):
    if "categories" in content:
        if "BSI" not in content["categories"]:
            content["categories"].append("BSI")
            logging.info("Auto-added category BSI: " + content["name"])
    else:
        content["categories"] = ["BSI"]
        logging.info("Auto-added categories section: " + content["name"])
        logging.info("Auto-added category BSI: " + content["name"])
    return content

def stackrox_manual():
    # if the implementationType is manual we check, that a file exists with the correct filename
    # generate filename
    filename = policy_dir + item['filename'].lower() + '.json'
    if os.path.isfile(filename):
        logging.info("file exists: " + filename)

        with open(filename, 'r') as manual_policy:
            policies_content = json.load(manual_policy)

        # We add a category BSI to Bundle all Policies
        for policy in policies_content["policies"]:
            policy = fix_bsi_category(content=policy)

        # we write changes to the file. We do not check if we had any changes, this way we can also unify indentation
        with open(filename, 'w') as manual_policy:
            json.dump(policies_content, manual_policy, indent=2)
    else:
        logging.error("file does not exist: " + filename)

def stackrox_upstream():
 with urllib.request.urlopen(item['stackrox']['src']) as orig_content:
    content = json.load(orig_content)

    # We add a category BSI to Bundle all Policies
    content = fix_bsi_category(content)

    # We overwrite attributes with the mirror values, if specified
    for attribute in ["name", "description", "rationale", "remediation"]:
        if len(item[attribute]) != 0 and content[attribute]:
            content[attribute] = item[attribute]

    # We overwrite attributes specific to stackrox
    for rox_attribute in ["id"]:
        if len(item["stackrox"][rox_attribute]) != 0 and content[rox_attribute]:
            content[rox_attribute] = item["stackrox"][rox_attribute]

    # We change to nonDefault policy
    content["isDefault"] = False
    if "disabled" in item["stackrox"]:
        content["disabled"] = item["stackrox"]["disabled"]
    else:
        content["disabled"] = policy_disabled_default

    # Overwriting, if necessary
    if "overwrite" in item["stackrox"]:
        if len(item["stackrox"]["overwrite"]["policySections"]) != 0:
            content["policySections"] = item["stackrox"]["overwrite"]["policySections"]

    # write policy into policies list, to have a importable format
    policy_dict = {'policies': [content,]}

    # generate filename from name
    outfilename = policy_dir + item['filename'].lower() + '.json'

    with open(outfilename, 'w') as json_file:
        json.dump(policy_dict, json_file, indent=2)

def create_stackrox_bundle():
    policy_list = []
    for policy in mirror_config['policies']:
        # we cant parse files we dont implement, so we skip them
        if policy["stackrox"]["implementationType"] == "none":
            logging.info("processing: " + policy["policyID"] + " as none - SKIPPED")
            continue
        else:
            # we construct the filename
            file = policy_dir + policy['filename'] + ".json"
            logging.info('processing:' + file)
            # check if the file exists, otherwise log error, we should have a file for every implemented policy
            if not os.path.isfile(file):
                logging.error(file + " not found")
                continue
            else:
                # if we have afile, we can open it, extract the policies included and add them to our list
                with open(file, 'r') as pol_file:
                    pol_file_content = json.load(pol_file)
                    # since some of the policies are manually constructed, we check if the field we need exists.
                    if "policies" in pol_file_content:
                        # for each element in the policies list, we add it to the bundle
                        policy_list.extend(pol_file_content['policies'])
                    else:
                        logging.error(file + " has wrong json format")

    # create bundle by writing the list to a json file
    bundle= {'policies': policy_list}
    with open(bundle_filename, 'w') as bundle_file:
        json.dump(bundle, bundle_file, indent=2)


# we iterate over every policy in the mirror_config
# download the src
# and modify the parameters
for item in mirror_config["policies"]:
    if item["stackrox"]["implementationType"] == "none":
        logging.info("processing: " + item["policyID"] + " as none - SKIPPED")
        continue
    elif item["stackrox"]["implementationType"] == "manual":
        logging.info("processing: " + item["policyID"] + " " + item["filename"] + " as manual")
        stackrox_manual()
    else:
        logging.info("processing: " + item["policyID"] + " " + item["filename"] + " as upstream")
        stackrox_upstream()


# create bundle
logging.info("Creating Bundle of all Policies")
create_stackrox_bundle()