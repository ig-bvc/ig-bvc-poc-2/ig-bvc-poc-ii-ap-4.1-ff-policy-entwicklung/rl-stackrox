Das Projekt ist nach: https://gitlab.opencode.de/ig-bvc/policy-entwicklung umgezogen

# Richtlinien-Umsetzung StackRox

## Zweck
Dieses Projekt stellt Policies für [StackRox](https://github.com/stackrox/stackrox) bereit, die auf den durch die Interessensgemeinschaft Betrieb von Containern (IG BvC) erarbeiteten [Richtlinien](https://gitlab.opencode.de/ig-bvc/ig-bvc-poc-2/ig-bvc-poc-ii-ap-4.1-ff-policy-entwicklung/richtlinien) basieren. Diese wiederum stellen eine mit Teilnehmern mehrerer Bundesbehörden und IT Dienstleistern von Ländern und Bund Interpretation von technisch Prüfbaren Maßnahmen der Bausteine SYS.1.6 und APP.4.4 des BSI (Bundesamt für Sicherheit in der Informationstechnik) bereit.

Das Projekt soll es ermöglichen, dass Firmen, Behörden oder auch Privatpersonen, die StackRox einsetzen, ein leicht zu importierendes, und anzupassendes Set an Policies haben, die sie wiederverwenden und anpassen können.
## Installation

###### 1. Auswählen der Policies, oder Nutzung des Bundles unter ./policies
###### 2. Anmeldung an StackRox
###### 3. Auswählen von Platform Configuration/Policy Management
![](docs/img/platformconfiguration-policymanagement.png)
###### 4. Auswählen von Import Policy
![](docs/img/import-policy.png)
###### 5. Copy & Paste des der Policies, oder Upload vom Dateisystem
![](docs/img/import-policy-json.png)
###### 6. Customizing der Policies

## Contributing
Jede Contribution ist willkommen. Für den einfacheren Einstieg hier ein paar Hinweise.

### Neue Policies
Es gibt technisch relevante Policies, die mit StackRox umsetzbar sind, die jedoch keinen BSI Bezug haben. Diese sind OUT OF SCOPE für das Projekt. Für jede zu implementierende Policy MUSS es eine ID im Richtlinienprojekt geben. Für eine IG BvC ID KANN es aber auch mehrere Policies geben, z.B. um mehrere Aspekte abzudecken/zu prüfen.

### Policy Typen und zentrale Logik
Es werden drei Typen der Policyimplementierung unterschieden:

`upstream` sind Policies, die schon bspw. in den Default Policies von StackRox vorhanden sind. Diese werden wiederverwendete um insbesondere Ausnahmen und Änderungen an StackRox selbst einfach übernehmen zu können ohne eigenen Aufwand zu haben. Die Policies werden durch das Script `policy-generator.py` heruntergeladen und verschiedene Felder wie Titel, Kategorie und andere überschrieben. Anschließend wird die Policy im Repository abgelegt. An diesen Policies soll keine händische Änderung erfolgen. Änderungen sollen über Overwrite-Definitionen in `policy-spec.yaml` implementiert werden.

`manual` sind Policies, für die keine Vorlagen existieren. Diese müssen im `policy-spec.yaml` hinterlegt sein, dass Script führt hier jedoch weniger Prüfungen und Änderungen durch.

`none` sind Policies, die nicht implementiert werden können, für die es jedoch eine Richtlinie der IG BvC gibt. Die Auflistung dieser Policies dient im Wesentlichen zur Vollständigkeitskontrolle.

Jede Policy sollte bei einem entsprechenden Testfall (-bad) auslösen, einen Testfall (-good) jedoch nicht.

### Testcases
Testfälle um die Policies zu testen, werden nicht in diesem Projekt geändert sondern kommen ebenfalls aus dem Richtlinien Projekt. Bei Fehlern in den Testfällen sind diese dort zu korrigieren.

### Acceptance Criteria
1. MergeRequest geht gegen den `dev` branch
2. Ein erneutes Ausführen von `policy-generator.py` löst keine Änderungen aus
3. Alle Policies sind in `policy-spec.yaml` hinterlegt.

## License
Das Projekt ist gemäß [LICENSE](LICENSE) lizensiert.

## Project status
Dieses Projekt ist in aktiver Entwicklung. Aktuell sind fast alle Richtlinien die technisch mit Stackrox abbildbar sind, als Policy vorhanden. Alle Issues oder MRs sind willkommen! Aktuell werden die Policies MANUELL gegen nur eine Kubernetes Distribution (OpenShift) getestet. Alternative Testumgebungen sind sehr willkommen!

## Roadmap
siehe Issues. Es wird jedoch versucht mehr Automation und Hands On Erfahrung in die Policies zu Bringen um den Pflegeaufwand gering und die Wiederverwendbarkeit hoch zu halten.